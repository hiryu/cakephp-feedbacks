<?php 
/* feedbacks schema generated on: 2012-04-06 04:39:30 : 1333679970*/
class feedbacksSchema extends CakeSchema {
	var $name = 'feedbacks';

	function before($event = array()) {
		return true;
	}

	function after($event = array()) {
	}

	var $feedback_items = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'primary'),
		'name' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 100, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'title' => array('type' => 'string', 'null' => true, 'default' => NULL, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1)),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'MyISAM')
	);
	var $feedback_ratings = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'primary'),
		'session_id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'index'),
		'item_id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'index'),
		'rating' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'length' => 2),
		'created' => array('type' => 'date', 'null' => false, 'default' => NULL),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1), 'session_id' => array('column' => 'session_id', 'unique' => 0), 'item_id' => array('column' => 'item_id', 'unique' => 0)),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'MyISAM')
	);
	var $feedback_sessions = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'primary'),
		'custom' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 11, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'comment' => array('type' => 'text', 'null' => false, 'default' => NULL, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'model' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 80, 'key' => 'index', 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'foreign_key' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'index'),
		'uuid' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 40, 'key' => 'index', 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'start' => array('type' => 'date', 'null' => false, 'default' => NULL),
		'end' => array('type' => 'date', 'null' => false, 'default' => NULL),
		'completed' => array('type' => 'integer', 'null' => false, 'default' => '0', 'length' => 4, 'key' => 'index'),
		'created' => array('type' => 'date', 'null' => true, 'default' => NULL),
		'modified' => array('type' => 'date', 'null' => true, 'default' => NULL),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1), 'completed' => array('column' => 'completed', 'unique' => 0), 'model' => array('column' => 'model', 'unique' => 0), 'foreign_key' => array('column' => 'foreign_key', 'unique' => 0), 'uuid' => array('column' => 'uuid', 'unique' => 0)),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'MyISAM')
	);
}
?>