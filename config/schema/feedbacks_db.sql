#feedbacks sql generated on: 2012-04-06 04:39:55 : 1333679995

DROP TABLE IF EXISTS `feedback_items`;
DROP TABLE IF EXISTS `feedback_ratings`;
DROP TABLE IF EXISTS `feedback_sessions`;


CREATE TABLE `feedback_items` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,	PRIMARY KEY  (`id`))	DEFAULT CHARSET=utf8,
	COLLATE=utf8_general_ci,
	ENGINE=MyISAM;

CREATE TABLE `feedback_ratings` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`session_id` int(11) NOT NULL,
	`item_id` int(11) NOT NULL,
	`rating` int(2) NOT NULL,
	`created` date NOT NULL,	PRIMARY KEY  (`id`),
	KEY `session_id` (`session_id`),
	KEY `item_id` (`item_id`))	DEFAULT CHARSET=utf8,
	COLLATE=utf8_general_ci,
	ENGINE=MyISAM;

CREATE TABLE `feedback_sessions` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`custom` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`comment` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`model` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`foreign_key` int(11) NOT NULL,
	`uuid` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`start` date NOT NULL,
	`end` date NOT NULL,
	`completed` int(4) DEFAULT 0 NOT NULL,
	`created` date DEFAULT NULL,
	`modified` date DEFAULT NULL,	PRIMARY KEY  (`id`),
	KEY `completed` (`completed`),
	KEY `model` (`model`),
	KEY `foreign_key` (`foreign_key`),
	KEY `uuid` (`uuid`))	DEFAULT CHARSET=utf8,
	COLLATE=utf8_general_ci,
	ENGINE=MyISAM;

