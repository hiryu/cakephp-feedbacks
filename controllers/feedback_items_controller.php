<?php
class FeedbackItemsController extends FeedbacksAppController {

	var $name = 'FeedbackItems';
	var $components = array('Session');

	/*
	 * Prefix admin
	 */
	function admin_index() {
		$this->set('feedbackItems', $this->paginate('FeedbackItem'));
	}

	function admin_add() {
		if (empty($this->data)) return;

		if ($this->FeedbackItem->save($this->data)) {
			$this->Session->setFlash(__d('feedbacks_plugin', 'Feedback item added', true));
			$this->redirect(array('action' => 'admin_index'));
		} else {
			$this->Session->setFlash(__d('feedbacks_plugin', 'Feedback item not added', true));
			$this->redirect($this->referer('/'));

		}
	}

	function admin_edit($id=null) {
		if ($id == null) {
			$this->Session->setFlash(__d('feedbacks_plugin', 'Id required', true));
		} elseif (empty($this->data)) { 
			$this->data = $this->FeedbackItem->read(null, $id);
		} else {
			if ($this->FeedbackItem->save($this->data)) {
				$this->Session->setFlash(__d('feedbacks_plugin', 'Feedback item updated', true));
				$this->redirect(array('action' => 'admin_index'));
			} else {
				$this->Session->setFlash(__d('feedbacks_plugin', 'Feedback item not updated', true));
				$this->redirect($this->referer('/'));
			}
		}
	}

	function admin_delete($id=null) { 
		if (empty($this->data) || $id == null) {
			$this->Session->setFlash(__d('feedbacks_plugin', 'Id required', true));
		}

		if ($this->FeedbackItem->delete($id)) {
			$this->Session->setFlash(__d('feedbacks_plugin', 'Feedback item deleted', true));
		} else {
			$this->Session->setFlash(__d('feedbacks_plugin', 'Feedback item not deleted', true));
		}
		$this->redirect(array('action' => 'admin_index'));
	}



}
