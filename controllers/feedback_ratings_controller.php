<?php
class FeedbackRatingsController extends FeedbacksAppController {

	var $name = 'FeedbackRatings';
	var $components = array('Session');


	function survey($hash=null) {
		$today = date('Y-m-d', time());
		$conditions = array('uuid' => $hash, 0 => "'{$today}' BETWEEN start AND end");
		if (!($session = $this->FeedbackRating
				->FeedbackSession
				->find('first', compact('conditions')))) {
			$this->Session->setFlash(__d('feedbacks_plugin', 'Feedback hash not exists or is expired', true));
			$this->redirect($this->redirect('/'));
		} elseif ((int) $session['FeedbackSession']['completed'] == 1) {
			$this->Session->setFlash(__d('feedbacks_plugin', 'You have already completed this survey. Thank you!', true));
			$this->redirect($this->redirect('/'));
		}
 		/* Estraggo elementi survey */
		$feedbackItems = $this->FeedbackRating->FeedbackItem->find('list');
		$this->set(compact('feedbackItems'));

		if (empty($this->data)) return;
		/* Forzo session_id (per motivi di sicurezza) */
		$sid = (int) $session[$this->FeedbackRating->FeedbackSession->alias]['id'];
		$this->data['FeedbackSession']['id'] = $sid;
		$this->data['FeedbackSession']['completed'] = 1;
		/* Aggiunto session_id a ogni record FeedbackRating */
		$setSessionId = create_function('&$data, $key, $sid', "return \$data['session_id'] = '$sid' ;");
		array_walk($this->data['FeedbackRating'], $setSessionId, $sid);
		if ($this->FeedbackRating->FeedbackSession->saveAll($this->data)) {
			$this->Session->setFlash(__d('feedbacks_plugin', 'Thank you for complete this survey.', true));
			$this->redirect('/');
		}
	}



	/*
	 * Prefix admin
	 */
	function admin_index() {
		$this->set('feedbackRatings', $this->paginate('FeedbackRating'));
	}

	/*function admin_add() {
		if (empty($this->data)) return;

		if ($this->FeedbackRating->save($this->data)) {
			$this->Session->setFlash(__d('feedbacks_plugin', 'Rating index added', true));
			$this->redirect(array('action' => 'admin_index'));
		} else {
			$this->Session->setFlash(__d('feedbacks_plugin', 'Rating index not added', true));
			$this->redirect($this->referer('/'));

		}
	}

	function admin_edit($id=null) {
		if ($id == null) {
			$this->Session->setFlash(__d('feedbacks_plugin', 'Id required', true));
		} elseif (empty($this->data)) { 
			$this->data = $this->FeedbackRating->read(null, $id);
			$this->set('feedbackRating');
		} else {
			if ($this->FeedbackRating->save($this->data)) {
				$this->Session->setFlash(__d('feedbacks_plugin', 'Rating index added', true));
				$this->redirect(array('action' => 'admin_index'));
			} else {
				$this->Session->setFlash(__d('feedbacks_plugin', 'Rating index not added', true));
				$this->redirect($this->referer('/'));
			}
		}
	}

	function admin_delete($id=null) { 
		if (empty($this->data) || $id == null) {
			$this->Session->setFlash(__d('feedbacks_plugin', 'Id required', true));
		}

		if ($this->FeedbackRatings->delete($id)) {
			$this->Session->setFlash(__d('feedbacks_plugin', 'Rating deleted', true));
		} else {
			$this->Session->setFlash(__d('feedbacks_plugin', 'Rating not deleted', true));
		}
		$this->redirect(array('action' => 'admin_index'));
	}
	*/


}
