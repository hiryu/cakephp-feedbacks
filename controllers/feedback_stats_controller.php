<?php
Class FeedbackStatsController extends FeedbacksAppController {
	var $name = 'FeedbackStats';
	var $uses = null;
	var $components = array('Session');
	var $helpers = array('Form', 'Paginator');

	var $paginate = array(
		'FeedbackSession' => array(
			'conditions' => array(
				'TRIM(FeedbackSession.comment) <> ""'
			),
			'recursive' => 0,
			'limit' => 20,
			'order' => array(
				'modified'
			)
		),
	);

	function beforeFilter() {
		$this->FeedbackRating = ClassRegistry::init('Feedbacks.FeedbackRating');
		$this->FeedbackSession = $this->FeedbackRating->FeedbackSession;
	}

	function get($model, $foreign_key) {
		if (empty($model) || empty($foreign_key)) {
			$this->Session->setFlash(__d('feedbacks_plugin', 'Give me a model and foreign_key', true));
			$this->redirect($this->referer('/'));
		}
		$group = array('FeedbackRating.item_id');
		$fields = '*, COUNT(*) AS feedbacks, ROUND(AVG(rating)) AS rating_avg';
		$conditions['model'] = Inflector::camelize(Inflector::singularize($model));
		$conditions['foreign_key'] = (int) $foreign_key;
		$filters = compact('fields', 'conditions', 'group');
		$stats = $this->FeedbackRating->find('all', $filters);
		/* Get relational model */
		$relationalModelClass = $this->FeedbackRating->FeedbackSession
					  			     ->_getModelClass($conditions['model']);
		if ($relationalModelClass !== false) {
			$entry = $relationalModelClass->read(null, $conditions['foreign_key']);
			$related['className'] = $relationalModelClass->alias;
			$related['displayField'] = $relationalModelClass->displayField;
			$related['title'] = $entry[$relationalModelClass->alias][$related['displayField']]; 
			$survey = Set::merge(array('related' => $related), array('related' => $entry));
		}
		/* Get survey summary */
		$conditions['FeedbackSession.completed'] = 1;
		$survey['summary']['completed'] = $this->FeedbackRating->FeedbackSession->find('count', compact('conditions'));
		//$survey['summary']['completed'] = sizeof(Set::extract('/FeedbackSession[completed=1]', $stats));
		unset($conditions['FeedbackSession.completed']);
		$survey['summary']['total'] = $this->FeedbackRating->FeedbackSession->find('count', compact('conditions'));
		$survey['summary']['uncompleted'] = $survey['summary']['total'] - $survey['summary']['completed'];
		
		$feedbacks = $this->paginate('FeedbackSession', $conditions);
		$this->set(compact('survey', 'stats', 'feedbacks'));
	}


}