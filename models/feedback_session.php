<?php
class FeedbackSession extends FeedbacksAppModel {
	var $name = 'FeedbackSession';
	var $displayField = 'uuid';
	var $validate = array(
		'custom' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'model' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'foreign_key' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'uuid' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'start' => array(
			'isDay' => array(
				'rule' => 'date'
			)
		),
		'end' => array(
			'isDay' => array(
				'rule' => 'date'
			)
		),
		'completed' => array(
			'isBool' => array(
				'rule' => 'boolean',
				'allowEmpty' => false
			)
		)
	);


	var $hasMany = array(
		/*'FeedbackItem' => array(
			'className' => 'Feedbacks.FeedbackItem',
			'foreign_key' => 'item_id',
			'dependent' => true
		),*/
		'FeedbackRating'  => array(
			'className' => 'Feedbacks.FeedbackRating',
			'foreignKey' => 'session_id',
			'dependent' => true
		),
	);



/**
 * Get related mode of the current Session
 * 
 * @param  str $model=null Model name
 * @return mixed            
 */
	public function _getModelClass($model=null) {
		$modelName = $this->id && is_null($model) ? $this->model : $model;
		if ($modelName) {
			/* import class file */
			if (App::import('Model', $modelName)) {
				return ClassRegistry::init($modelName);
			} else return null;
		}
		return null;
	}

/**
 * Get Model data for the related model of current FeedbackSession 
 * 
 * @param  str $model=null Model name
 * @param  int $fk=null    Foreign key
 * @return mixed             
 */
	public function _getModelEntry($model=null, $fk=null) {
		$modelName = $this->id && is_null($model) ? $this->model : $model;
		if ($modelName) {
				/* import class file */
				$Model = $this->_getModelClass($model);
				if ($Model != false) {
					return $Model->read(null, $fk);
				} else return null;
		}
		return null;
	}

}
