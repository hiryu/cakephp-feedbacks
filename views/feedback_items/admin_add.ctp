<?php echo 
	$this->Form->create('FeedbackItem')."\n".
	$this->Form->inputs(
		array(
			'legend' => __d('feedbacks_plugin', 'Add new rating item', true),
			'name',
			'title'
		)
	)."\n".
	$this->Form->end(__d('feedbacks_plugin', 'Add', true))
?>