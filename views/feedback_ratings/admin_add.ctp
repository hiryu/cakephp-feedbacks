<?php echo 
	$this->Form->create('FeedbackRating', array('url' => array( 'action' => $this->action)))."\n".
	$this->Form->inputs(
		array(
			'legend' => __d('feedbacks_plugin', 'Add new rating section', true),
			'name',
			'title'
		)
	)."\n".
	$this->Form->end(__d('feedbacks_plugin', 'Add', true))
?>