<?php
  $this->Html->script(array(
  	'http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js',
  	'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/jquery-ui.min.js',
  	//'/feedbacks/js/jqueryui-stars/jquery.ui.stars.js',
  	'/feedbacks/js/jqueryui-stars/jquery.ui.stars-include-jquery1.6.patch.js'
  ), array('inline' => false));
  echo $this->Html->css(array(
  	'http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/themes/ui-lightness/jquery-ui.css',
  	'/feedbacks/js/jqueryui-stars/css/jquery.ui.stars.css'
  ));
?>
<script>
	$(function () { 
		
		$(".jqueryui-starts-wrapper").stars({
			inputType: "select",
			cancelShow: false,
			//oneVoteOnly: true,
			//split: 2,
			//captionEl: $(this).find('label')
		});
		
	})
</script>

<?php
	echo $this->Form->create('FeedbackRating', array(
			'url' => Router::url('', true)
	));
	echo $this->Form->inputs(
		array(
			'legend' => __d('feedbacks_plugin', 'Feedbacks for ...', true),
			//'FeedbackSession.id' => array('value' => '2', ),
			//'FeedbackSession.hash' => array('value' => 'AAA-BBB-CCC-DDD'),
			'FeedbackSession.comment',
			'FeedbackSession.custom' => array(
				'options' => array(0 => 'foo', 1 => 'bar'),
				'empty' => 'Select a value..'
			)
		)
	);
	?>
	<?php $i = -1; foreach($feedbackItems as $pk => $item) : $i++ ?>
	  <?php 
	  		echo $this->Form->hidden(
	  			"FeedbackRating.{$i}.item_id", 
	  			array('value' => $pk)
	  		);
	  		echo $this->Form->input(
	  			"FeedbackRating.{$i}.rating", 
		  		array(
		  			'options' => range(0,10),
		  			'label' => $item,
		  			'default' => '5',
		  			'div' => array(
		  				'class' => 'jqueryui-starts-wrapper'
		  			),
		  			//'empty' => 'Select..'
		  			//'type' => 'select',
	  		));
	  ?>
	<?php endforeach ?>
	<?php
	echo $this->Form->end(__d('feedbacks_plugin', 'Save', true));
?>