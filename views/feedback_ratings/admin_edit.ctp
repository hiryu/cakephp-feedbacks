<div class="feedbackRatings form">
  <?php 
    echo $this->Form->create('FeedbackRating');
    echo $this->Form->inputs(
            array(
                'legend' => __d('feedbacks_plugin', 'Edit rating', true),
                'name',
                'title'
            )
        );
    echo $this->Form->end(__('Submit', true));
    ?>
</div>
<div class="actions">
    <h3><?php __('Actions'); ?></h3>
    <ul>

        <li><?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $this->Form->value('FeedbackRating.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $this->Form->value('FeedbackRating.id'))); ?></li>
        <li><?php echo $this->Html->link(__('List Feedback Ratings', true), array('action' => 'index'));?></li>
        <li><?php echo $this->Html->link(__('List Feedback Sessions', true), array('controller' => 'feedback_sessions', 'action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('New Session', true), array('controller' => 'feedback_sessions', 'action' => 'add')); ?> </li>
    </ul>
</div>