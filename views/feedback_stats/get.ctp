
<table>
	<thead>
		<caption>
			<?php 
		  		printf(
		  				__d('feedbacks_plugin', 'Feedbacks for: %s', true), 
		  				$survey['related']['title']) 
		  ?>
		</caption>
		<tr>
			<th><?php __d('feedbacks_plugin', 'Subject') ?></th>
			<th><?php __d('feedbacks_plugin', 'Average') ?></th>
			<th><?php __d('feedbacks_plugin', 'Replies') ?></th>
		</tr>
	</thead>
	<tfoot>
	 <tr><td colspan="3"><?php __d('feedbacks_plugin', 'Ratings board') ?></td></tr>
	</tfoot>
	<tbody>
	<?php foreach($stats as $item) : ?>
		<tr>
			<td><?php echo $item['FeedbackItem']['name'] ?></td>
			<td><?php echo $item[0]['rating_avg'] ?></td>
			<td>
				<?php
					 printf(
						'%d/%d (%d unrated)', 
						$survey['summary']['completed'], 
						$survey['summary']['total'],
						$survey['summary']['uncompleted']
					) 
				?>
			</td>
		</tr>
	<?php endforeach ?>
	</tbody>
</table>


<table>
	<thead>
		<caption>
			<?php 
				printf(
					__d('feedbacks_plugin', 'Feedbacks about: %s', true), 
		  		$survey['related']['title']
		  	) 
			?>
		</caption>
		<tr>
			<th><?php echo $this->Paginator->sort(__d('feedbacks_plugin', '#', true) , 'id') ?></th>
			<th><?php __d('feedbacks_plugin', 'Comment') ?></th>
			<th><?php echo $this->Paginator->sort(__d('feedbacks_plugin', 'Date', true) , 'modified') ?></th>				
		</tr>
	</thead>
	<tfoot>
	 <tr>
	 	<td colspan="3"><?php __d('feedbacks_plugin', 'Latest feedbacks') ?></td></tr>
	</tfoot>

	<tbody>
	 <?php foreach($feedbacks as $feedback) : ?>
		<tr>
			<td><?php echo $feedback['FeedbackSession']['id'] ?></td>
			<td><?php echo $feedback['FeedbackSession']['comment'] ?></td>
			<td><?php echo $feedback['FeedbackSession']['modified'] ?></td>
		</tr>
	<?php endforeach ?>
	</tbody>
</table>

<!-- Shows the page numbers -->
<?php echo $this->Paginator->numbers(); ?>
<!-- Shows the next and previous links -->
<?php echo $this->Paginator->prev('« Previous', null, null, array('class' => 'disabled')); ?>
<?php echo $this->Paginator->next('Next »', null, null, array('class' => 'disabled')); ?> 
<!-- prints X of Y, where X is current page and Y is number of pages -->
<?php echo $this->Paginator->counter(); ?>